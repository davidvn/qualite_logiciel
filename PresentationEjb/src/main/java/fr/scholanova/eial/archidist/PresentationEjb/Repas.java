package fr.scholanova.eial.archidist.PresentationEjb;

public class Repas {
	String nom;
	Double prix;
	Boolean dehors;
	
	public Repas (String nom, Double prix, Boolean dehors) {
		this.nom = nom;
		this.prix = prix;
		this.dehors = dehors;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Double getPrix() {
		return this.prix;
	}
	
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	
	public Boolean isDehors() {
		return this.dehors;
	}
	
	public void setNom(Boolean dehors) {
		this.dehors = dehors;
	}
}
