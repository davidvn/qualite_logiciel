package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.scholanova.eial.archidist.PresentationEjbInterface.IEjbTestEtat;

@Remote(IEjbTestEtat.class)
@Stateless
public class EjbStateless implements IEjbTestEtat {
private int nb;
	
	@Override
	public void increment() {
		nb++;
	}
	
	@Override
	public int getNb() {
		return this.nb;
	}
}
