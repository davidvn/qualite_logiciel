package fr.scholanova.eial.archidist.PresentationEjbClient;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // System.out.println( "Hello World!" );
        
        Properties props = new Properties();
        
        props.put(Context.PROVIDER_URL , "http-remoting://127.0.0.1:8080");
        props.put(Context.SECURITY_PRINCIPAL , "admin");
        props.put(Context.SECURITY_CREDENTIALS, "admin");
        props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        props.put("jboss.naming.client.ejb.context", true);
        
    	Context ctx;
    	
		try {
			ctx = new InitialContext(props);
			CalculInterface proxy = (CalculInterface) ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/Calcul!fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface");
			System.out.println(proxy.ajouter(2, 2));
	    	System.out.println(proxy.puissance(2, 2));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
